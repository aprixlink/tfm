/*
 * Copyright (C) 2015 Diego Ramirez dranklink@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package tfm.Model;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Esta interfaz define los metodos que se van a utilizar para hacer la 
 * conexion con la base de datos sin importar el motor de base de datos que se
 * utilice en el proyecto.
 * 
 * @author Diego Ramirez dranklink@gmail.com
 * @version 1.0
 * @since 12/04/2015
 */
public interface IDatabase {
    
    /**
     * Retorna si esta habilitada la conexion con la base de datos.
     * @return Retorna el estado de la conexion con la base de datos.
     */
    boolean Enabled();
    
    /**
     * Abre una conexion con la base de datos.
     * 
     * @param host Direccion de la base de datos. Ejemplo: 127.0.0.1.
     * @param port Puerto de la base de datos. Ejemplo 5432.
     * @param databaseName Nombre de la base de datos. Ejemplo moodle.
     * @param username Nombre de usuario para acceder a la base de datos.
     * @param password Contraseña del usuario para acceder a la base de datos.
     * @return Cadena vacia si se ejecuto con exito, en otro caso el mensaje de error.
     */
    String Open(String host, int port, String databaseName, String username, String password);
    
    /**
     * Abre una conexion con la base de datos, con el puerto por defecto.
     * 
     * @param host Direccion de la base de datos. Ejemplo: 127.0.0.1.
     * @param databaseName Nombre de la base de datos. Ejemplo moodle.
     * @param username Nombre de usuario para acceder a la base de datos.
     * @param password Contraseña del usuario para acceder a la base de datos.
     * @return Cadena vacia si se ejecuto con exito, en otro caso el mensaje de error.
     */
    String Open(String host, String databaseName, String username, String password);
    /**
     * Cierra la conexion actual con la base de datos.
     */
    
    /**
     * Cierra la conexion actual con la base de datos.
     */
    void Close();
    /**
     * Ejecuta la sentencia en la base de datos.
     * 
     * @param query Sentencia a ejecutar.
     * @return El resultado de la consulta. 
     * @throws java.sql.SQLException Puede regresar errores ejecutando la consulta.
     */
    ResultSet ExecuteQuery(String query) throws SQLException;
    
    /**
     * Prepara un query para que se le pueda añadir de forma segura parametros.
     * @param statement Query a ejecutar.
     * @return Una instancia de PreparedStatement.
     * @throws java.sql.SQLException Puede regresar errores creando la consulta.
     */
    PreparedStatement Get(String statement)  throws SQLException ;
    
    /**
     * Nombre del motor de base de datos. Ejemplo: MySQL.
     * @return Nombre del motor de base de datos.
     */
    String GetName();
    
    /**
     * Puerto por defecto usado en esa base de datos.
     * @return El puerto.
     */
    int DefaultPort();
}
