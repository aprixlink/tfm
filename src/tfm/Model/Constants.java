/*
 * Copyright (C) 2015 Diego Ramirez dranklink@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package tfm.Model;

/**
 * Clase que contiene valores constantes del proyecto.
 *
 * @author Diego Ramirez dranklink@gmail.com
 * @version 1.0
 * @since 12/04/2015
 */
public class Constants {
    /**
     * Cadena vacia.
     */
    public static final String STRING_EMPTY = "";
    
    /**
     * Curso inicial de Moodle.
     */
    public static final int INITIAL_COURSE = 1;
}
