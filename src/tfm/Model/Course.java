/*
 * Copyright (C) 2015 Diego Ramirez dranklink@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package tfm.Model;

/**
 * Clase que representa los cursos de la base de datos.
 * 
 * @author Diego Ramirez dranklink@gmail.com
 * @version 1.0
 * @since 1/05/2015
 */
public class Course {

    /**
     * Id del curso.
     */
    public int Id;

    /**
     * Nombre completo del curso.
     */
    public String FullName;

    /**
     * Nombre corto del curso.
     */
    public String ShortName;
    
    /**
     * Constructor de la clase.
     * @param id Id del curso.
     * @param fullname Nombre completo del curso.
     * @param shortname Nombre corto del curso.
     */
    public Course(int id, String fullname, String shortname){
        Id = id;
        FullName = fullname;
        ShortName = shortname;
    }
    
    @Override
    public String toString()
    {
        return FullName + " (" + ShortName+" )";
    }
}
