/*
 * Copyright (C) 2015 Diego Ramirez dranklink@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package tfm.Model.helper;

import java.util.ArrayList;

/**
 * Clase auxiliar que maneja las columnas de un registro provenientes de la query principal.
 *
 * @author Diego Ramirez dranklink@gmail.com
 * @version 1.0
 * @since 2/05/2015
 */
public class ResultRow {
    /**
     * Campos del registro.
     */
    public ArrayList<String> Fields;
    
    /**
     * Retorna una lista de los campos de la consulta.
     */
    public ResultRow(){
        Fields = new ArrayList<>();
    }
}
