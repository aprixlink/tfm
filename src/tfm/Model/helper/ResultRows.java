/*
 * Copyright (C) 2015 Diego Ramirez dranklink@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package tfm.Model.helper;

import java.util.ArrayList;

/**
 * Clase auxiliar que maneja los resultados de la query principal.
 *
 * @author Diego Ramirez dranklink@gmail.com
 * @version 1.0
 * @since 2/05/2015
 */
public class ResultRows {
    /**
     * Columnas de la consulta.
     */
    public ArrayList<String> Columns;
    /**
     * Registros del resultado de la query principal.
     */
    public ArrayList<ResultRow> Rows;
    
    /**
     * Retorna una lista de los registros de la consulta.
     */
    public ResultRows(){
        Rows = new ArrayList<>();
        Columns = new ArrayList<>();
    }
}
