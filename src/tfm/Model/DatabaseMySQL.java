/*
 * Copyright (C) 2015 Diego Ramirez dranklink@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package tfm.Model;

import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import tfm.Main;

/**
 * Clase que define una conexion con una base de datos tipo MySQL.
 * 
 * @author Diego Ramirez dranklink@gmail.com
 * @version 1.0
 * @since 12/04/2015
 */
public class DatabaseMySQL implements IDatabase{

    private Connection connection;
    private Statement statement;
    private boolean enabled;
    
    @Override
    public String Open(String host, int port, String databaseName, String username, String password) {
        String urlHost = String.format("jdbc:mysql://%s:%s/%s", host, port, databaseName);
        try {
            connection = DriverManager.getConnection(urlHost, username, password);
            statement = connection.createStatement(); 
            enabled = true;
            return Constants.STRING_EMPTY;
        } catch (SQLException ex) {
            if (Main.IsLogActived){
                Logger.getLogger(DatabaseMySQL.class.getName()).log(Level.SEVERE, null, ex);
            }
            return ex.getMessage();
        }
    }
    
    @Override
    public String Open(String host, String databaseName, String username, String password) {
        return Open(host, DefaultPort(), databaseName, username, password);
    }

    @Override
    public void Close() {
        if(connection != null){
            try {
                connection.close();
            } catch (SQLException ex) {
                if (Main.IsLogActived){
                    Logger.getLogger(DatabaseMySQL.class.getName()).log(Level.SEVERE, null, ex);
                }
            }finally{
                enabled = false;
            }
        }
    }

    @Override
    public ResultSet ExecuteQuery(String query) throws SQLException{
        if(statement != null){
            return statement.executeQuery (query);
        }else{
            return null;
        }
    }
    
    @Override
    public String GetName() {
        return "MySQL";
    }
    
    @Override
    public int DefaultPort() {
        return 3306;
    }

    @Override
    public boolean Enabled() {
        return enabled;
    }
    
    @Override
    public PreparedStatement Get(String statement) throws SQLException {
        return connection.prepareStatement(statement);
    }

}
