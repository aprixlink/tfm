/*
 * Copyright (C) 2015 Diego Ramirez dranklink@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package tfm.Model;

import tfm.Model.helper.ResultRows;
import tfm.Model.helper.ResultRow;
import Language.LanguagesController;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import tfm.Main;

/**
 * Clase que se encarga de manejar todas las clases en el modelo.
 *
 * @author Diego Ramirez dranklink@gmail.com
 * @version 1.0
 * @since 12/04/2015
 */
public class Manager {
    
    private IDatabase[] databaseList;
    private IDatabase databaseSelect;
    private static LanguagesController l;
    private String prefix;
    private final String formatDate = "yyyy-MM-dd HH:mm:ss";
    private String logTable;
    /**
     * Lista que muestra los cursos registrados en Moodle.
     */
    public ArrayList<Course> CourseList = new ArrayList<>();
    //public ArrayList<User> UserList = new ArrayList<>();
    private ResultRows lastResult = new ResultRows();
    

    /**
     * @return Lista de las base de datos.
     */
    public IDatabase[] getDatabaseList() {
        return databaseList; 
    }
    
    /**
     *  Constructor de la clase.
     */
    public Manager(){
        RegisterDatabases();
        l = LanguagesController.Instance();
    }
    
    /**
     * Registra los motores de base de datos.
     */
    private void RegisterDatabases(){
        databaseList = new IDatabase[]{
            new DatabasePostgreSQL(),
            new DatabaseMySQL()
        };
    }
    
    /**
     * Ejecuta los procedimiento para hacer el login en la base de datos
     * @param username Nombre de usuario.
     * @param password Contraseña del usuario.
     * @param host Ip del servidor de base de datos.
     * @param port Puerto del servidor de base de datos.
     * @param databaseName Nombre de la base de datos.
     * @param databaseType Tipo de base datos.
     * @return Vacio si fue exitoso, sino el mensaje de error.
     */
    public String Login(String username, String password, String host, int port, String databaseName, IDatabase databaseType){
        databaseSelect = databaseType;
        return databaseSelect.Open(host, port, databaseName, username, password);
    }
    
    /**
     * Busca en la base de datos si las tablas empienzan con el prefijo.
     * @param prefix Prefijo a buscar.
     * @param logTable Tabla Log.
     * @return Vacio si fue exitoso, sino el mensaje de error.
     */
    public String CheckPrefix(String prefix, String logTable){
        if(!databaseSelect.Enabled()){
            return l.getWord("Database conexion enabled.");
        }
        try {
            ResultSet r = databaseSelect.ExecuteQuery("SELECT table_name FROM information_schema.tables WHERE table_type='BASE TABLE';");
            while (r.next()) {
                if(r.getString("table_name").equals(prefix+logTable)){
                    this.prefix = prefix;
                    this.logTable = logTable;
                    return Constants.STRING_EMPTY; 
                }
            }
            return l.getWord("Prefix not found."); 
        } catch (SQLException ex) {
            if (Main.IsLogActived){
                Logger.getLogger(Manager.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return Constants.STRING_EMPTY;
    }
    
    /**
     * Metodo comun que se encarga de hacer todos los llamados y validar que se pueda ejecutar sin problemas las consultas.
     * 
     * @param username Nombre de usuario.
     * @param password Contraseña del usuario.
     * @param host Ip del servidor de base de datos.
     * @param port Puerto del servidor de base de datos.
     * @param databaseName Nombre de la base de datos.
     * @param databaseType Tipo de base datos.
     * @param logTable Tabla log en la cual se ejecutaran las consultas.
     * @param prefix Prefijo a buscar en las tablas de la base de datos.
     * @return Vacio si fue exitoso, sino el mensaje de error.
     */
    public String CheckAll(String username, String password, String host, int port, String databaseName, IDatabase databaseType, String logTable, String prefix){
        String r = Login(username, password, host, port, databaseName, databaseType);
        
        if(r.equals(Constants.STRING_EMPTY) && databaseName.length()<1){
           r = l.getWord("Please check the database name."); 
        }
        
        if(r.equals(Constants.STRING_EMPTY)){
           r = CheckPrefix(prefix, logTable);
        }
        if(r.equals(Constants.STRING_EMPTY)){
            GetCourses();
        }
        if(r.equals(Constants.STRING_EMPTY)){
            //GetUsers();
        }
        return r;
    }
    
    private void GetCourses(){
        CourseList = new ArrayList<>();
        if(!databaseSelect.Enabled()){
            return;
        }
        try {
            ResultSet r = databaseSelect.ExecuteQuery("SELECT id, fullname, shortname FROM "+prefix+"course ORDER BY fullname;");
            while (r.next()) {
               CourseList.add(new Course(r.getInt("id"), r.getString("fullname"), r.getString("shortname")));
            }
        } catch (SQLException ex) {
            if (Main.IsLogActived){
                Logger.getLogger(Manager.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    /*
    private void GetUsers(){
        UserList = new ArrayList<>();
        if(!databaseSelect.Enabled()){
            return;
        }
        try {
            ResultSet r = databaseSelect.ExecuteQuery("SELECT id,username, firstname, lastname FROM "+prefix+"user;");
            while (r.next()) {
               UserList.add(new User(r.getInt("id"), r.getString("username"), r.getString("firstname"), r.getString("lastname")));
            }
        } catch (SQLException ex) {
            if (Main.IsLogActived){
                Logger.getLogger(Manager.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    */
    
    /**
     * Convierte de Cadena a Fecha
     * @param date Cadena de la Fecha a convertir
     * @return la fecha en cadena de texto.
     */
    public Date StringToDate(String date){
        DateFormat dateFormat = new SimpleDateFormat(formatDate);
        try {
           return dateFormat.parse(date);
        } catch (ParseException ex) {
            if (Main.IsLogActived){
                Logger.getLogger(Manager.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }
    
    /**
     * Convierte de Fecha a Cadena.
     * @param date Fecha a convertir.
     * @return la cadena de la fecha.
     */
    public String DateToString(Date date){
        DateFormat dateFormat = new SimpleDateFormat(formatDate);
        return dateFormat.format(date);
    }
    
    private String GetTimeQuery(){
        if(databaseSelect instanceof DatabasePostgreSQL){
            return "TO_CHAR(TO_TIMESTAMP(time), 'yyyy-MM-dd HH:mm:ss') ";
        }
        if(databaseSelect instanceof DatabaseMySQL){
            return "FROM_UNIXTIME(time, '%Y-%m-%d %h:%m:%s') ";
        }
        return Constants.STRING_EMPTY;
    }
    
    /**
     * Ejecuta la query principal.
     * @param course Curso seleccionado.
     * @param user Usuario seleccionado.
     * @param startingDate Fecha Inicial de la busqueda.
     * @param endingDate Fecha Final de la busqueda.
     * @return el resultado de la consulta.
     */
    public ResultRows Calculate(Course course, User user, Date startingDate, Date endingDate){
        if (course == null){
            course = new Course(-1, "", "");
        }
        if (user == null){
            user = new User(-1, "", "", "");
        }
        String logTableFinal = prefix+logTable;
        String whereCourse = "course = "+course.Id+" AND ";
        String dateTrunc = " AND "+GetTimeQuery()+"BETWEEN '"+DateToString(startingDate)+"' AND '"+DateToString(endingDate)+"' ";
        String query = "SELECT userid, COUNT(*) AS num_logins "
               
                + ",(SELECT COUNT(*) AS num_acc_foros FROM "+logTableFinal+" AS C WHERE C."+whereCourse+"C.userid = A.userid AND C.action = 'view forum' "+dateTrunc+" GROUP BY C.userid) AS num_acceso_foros "
                + ",(SELECT COUNT(*) AS num_add_post FROM "+logTableFinal+" AS D WHERE  D."+whereCourse+"D.userid = A.userid AND D.action = 'add post' "+dateTrunc+" GROUP BY D.userid) AS num_aportaciones_foros"
                + ",(SELECT COUNT(*) AS num_view_discussion FROM "+logTableFinal+" AS E WHERE  E."+whereCourse+"E.userid = A.userid AND E.action = 'view discussion' "+dateTrunc+" GROUP BY E.userid) AS num_acceso_posts"
                + ",(SELECT COUNT(*) AS num_accesos_glosarios FROM "+logTableFinal+" AS F WHERE  F."+whereCourse+"F.userid = A.userid AND F.module = 'glossary' and F.action = 'view' "+dateTrunc+" GROUP BY F.userid) AS num_acceso_glosarios"
                + ",(SELECT COUNT(*) AS num_entradas_glosario FROM "+logTableFinal+" AS G WHERE  G."+whereCourse+"G.userid = A.userid AND G.module = 'glossary' and G.action = 'add entry' "+dateTrunc+" GROUP BY G.userid) AS num_aportaciones_glosarios"
                + ",(SELECT COUNT(*) AS num_accesos_encuestas FROM "+logTableFinal+" AS H WHERE  H."+whereCourse+"H.userid = A.userid AND H.module = 'survey' and H.action = 'view form' "+dateTrunc+" GROUP BY H.userid) AS num_acceso_encuestas"
                + ",(SELECT COUNT(*) AS num_submit_encuestas FROM "+logTableFinal+" AS I WHERE  I."+whereCourse+"I.userid = A.userid AND I.module = 'survey' and I.action = 'submit' "+dateTrunc+" GROUP BY I.userid) AS num_envio_encuestas"
                + ",(SELECT COUNT(*) AS num_total_tareas FROM "+logTableFinal+" AS J WHERE  J."+whereCourse+"J.userid = A.userid AND J.module = 'assignment' "+dateTrunc+" GROUP BY J.userid) AS num_total_tareas"
                + ",(SELECT COUNT(*) AS num_consulta_tareas FROM "+logTableFinal+" AS K WHERE  K."+whereCourse+"userid = A.userid AND K.module = 'assignment' AND K.action = 'view' "+dateTrunc+" GROUP BY K.userid) AS num_consulta_tareas"
                + ",(SELECT COUNT(*) AS num_envios_tareas FROM "+logTableFinal+" AS L WHERE  L."+whereCourse+"L.userid = A.userid AND L.module = 'assignment' and L.action='upload' "+dateTrunc+" GROUP BY L.userid) AS num_envios_tareas"
                + ",(SELECT COUNT(*) AS num_accesos_cuestionarios FROM "+logTableFinal+" AS M WHERE  M."+whereCourse+"userid = A.userid AND M.module = 'quiz' AND M.action = 'view' "+dateTrunc+" GROUP BY M.userid) AS num_accesos_cuestionarios"
                + ",(SELECT COUNT(*) AS num_intentos_cuestionarios FROM "+logTableFinal+" AS N WHERE  N."+whereCourse+"userid = A.userid AND N.module = 'quiz' AND N.action = 'attempt' "+dateTrunc+" GROUP BY N.userid) AS num_intentos_cuestionarios"
                + ",(SELECT COUNT(*) AS num_preguntas_contestadas_cuestionarios FROM "+logTableFinal+" AS O WHERE  O."+whereCourse+"userid = A.userid AND O.module = 'quiz' AND O.action = 'continue attempt' "+dateTrunc+" GROUP BY O.userid) AS num_preguntas_contestadas_cuestionarios"
                + ",(SELECT COUNT(*) AS num_visualizaciones_resumen_cuestionarios FROM "+logTableFinal+" AS P WHERE P."+whereCourse+"userid = A.userid AND P.module = 'quiz' AND P.action = 'view summary' "+dateTrunc+" GROUP BY P.userid) AS num_visualizaciones_resumen_cuestionarios"
                + ",(SELECT COUNT(*) AS num_cierres_cuestionarios FROM "+logTableFinal+" AS Q WHERE  Q."+whereCourse+"userid = A.userid AND Q.module = 'quiz' AND Q.action = 'close attempt' "+dateTrunc+" GROUP BY Q.userid) AS num_cierres_cuestionarios"
                + ",(SELECT COUNT(*) AS num_revisiones_intento_cuestionarios FROM "+logTableFinal+" AS R WHERE  R."+whereCourse+"userid = A.userid AND R.module = 'quiz' AND R.action = 'review' "+dateTrunc+" GROUP BY R.userid) AS num_revisiones_intento_cuestionarios"
                + ""
                
                + " FROM "+logTableFinal+" AS A "
                + " WHERE course = "+ Constants.INITIAL_COURSE +" AND "               
                + GetTimeQuery()
                + "BETWEEN '"+DateToString(startingDate)+"' AND '"+DateToString(endingDate)+"' "
                + "GROUP BY A.userid;";
        
        System.out.println(query);
        ResultRows rows = new ResultRows();
        if(!databaseSelect.Enabled()){
            return rows;
        }
        try {
            ResultSet r = databaseSelect.ExecuteQuery(query);
            ResultSetMetaData metaData = r.getMetaData();
            int count = metaData.getColumnCount();
            
            for (int i = 1; i <= count; i++){
                rows.Columns.add(metaData.getColumnName(i));
            }
            ResultRow row;
            while (r.next()) {
                row = new ResultRow();
                for(String s : rows.Columns){
                    row.Fields.add(r.getString(s));
                }
                rows.Rows.add(row);
                System.out.println(r.getInt("userid"));
            }
        } catch (SQLException ex) {
            if (Main.IsLogActived){
                Logger.getLogger(Manager.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        lastResult = rows;
        return rows;
    }
    
    /**
     * Guarda la consulta en un archivo CSV.
     * @param file Ruta del archivo a guardar.
     * @return Vacio si fue exitoso, sino el mensaje de error.
     */
    public String SaveCSV(File file){
        try {
            String path = file.getAbsolutePath();
            if(!path.endsWith(".csv")){
                path = path + ".csv";
            }
            PrintWriter writer = new PrintWriter(path, "UTF-8");
            for(int i=0; i< lastResult.Columns.size(); i++){
                if(i<lastResult.Columns.size()-1){
                    writer.print(lastResult.Columns.get(i)+",");
                }else{
                    writer.println(lastResult.Columns.get(i));
                }
            }
            for(int i=0; i< lastResult.Rows.size(); i++){
                for(int j=0; j< lastResult.Rows.get(i).Fields.size(); j++){
                    if(CheckRow(lastResult.Rows.get(i).Fields)){
                        if(j<lastResult.Rows.get(i).Fields.size()-1){
                            String a = lastResult.Rows.get(i).Fields.get(j);
                            writer.print(lastResult.Rows.get(i).Fields.get(j)+",");
                        }else{
                            if(i<lastResult.Rows.size()-1){
                                writer.println(lastResult.Rows.get(i).Fields.get(j));
                            }else{
                                writer.print(lastResult.Rows.get(i).Fields.get(j));
                            }
                        }
                    }
                }
            }
            writer.close();
        } catch (FileNotFoundException | UnsupportedEncodingException ex) {
            if (Main.IsLogActived){
                Logger.getLogger(Manager.class.getName()).log(Level.SEVERE, null, ex);
            }
            return ex.toString();
        }
        return Constants.STRING_EMPTY;
    }
    
    private boolean CheckRow(ArrayList row){
        for (int i=2;i<row.size();i++){
            if (row.get(i) != null){
                return true;
            }
        }
        return false;
    }
    
}
