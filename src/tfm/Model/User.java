/*
 * Copyright (C) 2015 Diego Ramirez dranklink@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package tfm.Model;

/**
 * Clase que representa un usuario de Moodle.
 *
 * @author Diego Ramirez dranklink@gmail.com
 * @version 1.0
 * @since 1/05/2015
 */
public class User {

    /**
     * Id del usuario.
     */
    public int Id;

    /**
     * Usuario del estudiante.
     */
    public String Username;

    /**
     * Nombre del estudiante.
     */
    public String FirstName;

    /**
     * Apellido del estudiante.
     */
    public String LastName;
    
    /**
     * Constructor de la clase.
     * @param id Id del estudiante.
     * @param username Usuario del estudiante.
     * @param firtsname Nombre del estudiante.
     * @param lastname Apellido del estudiante.
     */
    public User(int id, String username, String firtsname, String lastname){
        Id = id;
        Username = username;
        FirstName = firtsname;
        LastName = lastname;
    }
}
