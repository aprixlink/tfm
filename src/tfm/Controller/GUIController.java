/*
 * Copyright (C) 2015 Diego Ramirez dranklink@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package tfm.Controller;

import Language.LanguagesController;
import java.awt.Component;
import java.util.Date;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import java.io.File;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import tfm.Model.Constants;
import tfm.Model.Course;
import tfm.Model.IDatabase;
import tfm.Model.Manager;
import tfm.Model.helper.ResultRow;
import tfm.Model.helper.ResultRows;
import tfm.View.GUI;

/**
 * Clase que controladora de la interfaz GUI
 * 
 * @author Diego Ramirez dranklink@gmail.com
 * @version 1.0
 * @since 18/04/2015
 */
public class GUIController {
    
    private static Manager m;
    private static GUI gui;
    private static LanguagesController l;

    /**
     * Panels que tiene la interfaz GUI.
     */
    public enum Panels { 

        /**
         * Panel que contiene la interfaz de los lenguajes.
         */
        Language, 

        /**
         * Panel que contiene la interfaz del conexion con la base de datos.
         */
        Login, 

        /**
         * Panel que contiene la interfaz donde se realizan las consultas.
         */
        Query }
    
    /**
     * Constructor de la clase.
     */
    public GUIController(){
        m = new Manager();
        gui = new GUI();
        gui.setVisible(true);
        l = LanguagesController.Instance();
        SetLanguages();
        SetDatabases();
        ShowPanel(Panels.Language);
    }
    
    private void SetLanguages(){
        for (String Get : l.Get()) {
            gui.Language_Selection.addItem(Get);
        }
    }
    
    private void SetDatabases(){
        for (IDatabase d : m.getDatabaseList()) {
            gui.Login_Database_Type.addItem(d.GetName());
        }
    }
    
    /**
     * Asigna el motor de base de datos a usar.
     * @param databaseType Tipo de motor de base de datos.
     */
    public static void SetDatabases(String databaseType){
        IDatabase databaseSelect = m.getDatabaseList()[0];
        for (IDatabase d : m.getDatabaseList()) {
            if(d.GetName().equals(databaseType)){
                databaseSelect = d;
                break;
            }
        }
        gui.Login_Port.setText(Integer.toString(databaseSelect.DefaultPort()));
    }
    
    /**
     * Asigna el lenguaje escogido.
     * @param key Llave del lenguaje.
     */
    public static void SetLanguage(String key){
        l.Set(key);
        ShowPanel(Panels.Login);
        SetText();
    }
    
    /**
     * Regresa a la pantalla de seleccion de lenguaje.
     */
    public static void BackToLanguage(){
        ShowPanel(Panels.Language);
    }
    
    /**
     *  Regresa a la pantalla de inicio de sesion en la base de datos.
     */
    public static void BackToLogin(){
        ShowPanel(Panels.Login);
    }
            
    private static void ShowPanel(Panels panel){
        gui.Login_Log.setText("");
        gui.Query_Error.setText("");
        DefaultTableModel dm = (DefaultTableModel) gui.Query_Result.getModel();
            int rowCount = dm.getRowCount();
            for (int i = rowCount - 1; i >= 0; i--) {
                dm.removeRow(i);
            }
            dm.setColumnCount(0);
        gui.Language_Panel.setVisible(false);
        gui.Login_Panel.setVisible(false);
        gui.Query_Panel.setVisible(false);
        switch(panel){
            case Language:
                gui.Language_Panel.setVisible(true);
                break;
            case Login:
                gui.Login_Panel.setVisible(true);
                break;
            case Query:
                gui.Query_Panel.setVisible(true);
                Date date = new Date();
                gui.Query_Date_S.setText("2000-00-00 00:00:00");
                gui.Query_Date_E.setText(m.DateToString(date));
                break;
        }
    }
    
    /**
     * Ejecuta el calculo de la sentencia principal.
     */
    public static void Calculate(){
        Course courseSelected = null;
        for (Course c : m.CourseList) {
            if(c == gui.Query_Course.getSelectedItem()){
                courseSelected = c;
            }
        }
        Date sd = m.StringToDate(gui.Query_Date_S.getText());
        Date ed = m.StringToDate(gui.Query_Date_E.getText());
        if(sd == null || ed == null){
            gui.Query_Error.setText(l.getWord("Error Format Datetime"));
        }else{
            DefaultTableModel dm = (DefaultTableModel) gui.Query_Result.getModel();
            int rowCount = dm.getRowCount();
            for (int i = rowCount - 1; i >= 0; i--) {
                dm.removeRow(i);
            }
            dm.setColumnCount(0);
            gui.Query_Error.setText(l.getWord(""));
            ResultRows result = m.Calculate(courseSelected, null, sd, ed);
            int index = -1;
            for(String s : result.Columns){
                index++;
                dm.addColumn(s);
            }
            for(int i=0; i <result.Columns.size();i++){
                gui.Query_Result.getColumnModel().getColumn(i).setPreferredWidth(250);
            }
            Object[] data;
            int index_r = -1;
            for(ResultRow r : result.Rows){
                index_r++;
                if(index_r>99){
                    break;
                }              
                index = -1;
                data = new Object[result.Columns.size()];
                for(String s : result.Columns){
                    index++;
                    data[index] = r.Fields.get(index);
                }
                dm.insertRow(index_r, data);
            }
            gui.Query_Result.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        }
    }

    
    private static void SetText(){
        SetText(gui.Login_Panel);
        SetTextButton(gui.Login_Panel);
        SetText(gui.Query_Panel);
        SetTextButton(gui.Query_Panel);
    }
   
    private static void SetText(JPanel panel){
        JLabel label;
        Component[] components = panel.getComponents();
        for (Component component : components) {
            if (component instanceof JLabel) {
                label = (JLabel) component;
                if (label.getName() == null || label.getName().length() < 1){
                    label.setName(label.getText());
                }     
               
                label.setText(l.getWord(label.getName()));
            }
        }
    }
    
    private static void SetTextButton(JPanel panel){
        JButton button;
        Component[] components = panel.getComponents();
        for (Component component : components) {
            if (component instanceof JButton) {
                button = (JButton) component;
                if (button.getName() == null || button.getName().length() < 1){
                    button.setName(button.getText());
                }
                button.setText(l.getWord(button.getName()));
            }
        }
    }
    
    /**
     * Ejecuta los procedimiento para hacer el login en la base de datos
     * @param username Nombre de usuario.
     * @param password Contraseña del usuario.
     * @param host Ip del servidor de base de datos.
     * @param port Puerto del servidor de base de datos.
     * @param databaseName Nombre de la base de datos.
     * @param databaseType Tipo de base datos.
     */
    public static void Login(String username, String password, String host, String port, String databaseName, String databaseType){
        IDatabase databaseSelect = null;
        for (IDatabase d : m.getDatabaseList()) {
            if(d.GetName().equals(databaseType)){
                databaseSelect = d;
                break;
            }
        }
        int intport = Integer.parseInt(port);
        String r = m.CheckAll(username, password, host, intport, databaseName, databaseSelect, gui.Login_Log_Table.getText(), gui.Login_Prefix.getText());
                
        if(r.equals(Constants.STRING_EMPTY)){
            r = l.getWord("Connection successful");
            m.CourseList.stream().forEach((c) -> {
                gui.Query_Course.addItem(c);
            });
            ShowPanel(Panels.Query);
        }
        gui.Login_Log.setText(r);
    }
    
    /**
     * Guardar la consulta en un archivo CSV.
     * @param file Ruta del archivo.
     */
    public static void SaveFileCSV(File file){
        String r = m.SaveCSV(file);
        if(!r.equals(Constants.STRING_EMPTY)){
            gui.Query_Error.setText(r);
        }else{
            gui.Query_Error.setText("");
        }
    }
}
